<?php
session_start();

require './vendor/autoload.php';
require 'bootstrap.php';

use App\Auth\AuthController;
use App\Client\ClientController;
use App\Client\ClientRepository;

$authController = new AuthController();
$clientController = new ClientController(new ClientRepository);
$requestUri = $_SERVER['REQUEST_URI'];
// Parse the URL
$parsedUrl = parse_url($requestUri);
// Extract the path
$path = $parsedUrl['path'];
// Extract the query string parameters
parse_str($parsedUrl['query'], $queryParams);
// Split the path into segments
$segments = explode('/', trim($path, '/'));

switch ($segments[0]) {
    case 'auth':
        $authController->handle();
        break;
    case 'clients':
        if (isset($segments[1]) && is_numeric($segments[1])) {
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $clientController->show($segments[1]);
            }else{
                $clientController->update($segments[1]);
            }
        } else {
            $clientController->index();
        }
        break;
}
