<?php

namespace App;

class BaseController
{

    public function __construct()
    {
    }

    function response($data = null, $code = 200, $message = '', $warnings = [])
    {
        $response = [
            "code" => $code,
            "message" => $message,
            "datas" => $data,
            "warnings" => $warnings
        ];

        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }
}
