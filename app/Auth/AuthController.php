<?php

namespace App\Auth;

use App\BaseController;
use Firebase\JWT\JWT;

class AuthController extends BaseController
{

    public function __construct()
    {
    }

    public function handle()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $api_url = 'https://evaluation-technique.lundimatin.biz/api/auth';
                $user_name = isset($_REQUEST['user_name']) ? $_REQUEST['user_name'] : '';
                $password = isset($_REQUEST['password']) ? $_REQUEST['password'] : '';
                $password_type = isset($_REQUEST['password_type']) ? $_REQUEST['password_type'] : '';
                $code_application = isset($_REQUEST['code_application']) ? $_REQUEST['code_application'] : '';
                $code_version = isset($_REQUEST['code_version']) ? $_REQUEST['code_version'] : '';
                $payload = [
                    "user_name" => $user_name,
                    "password_type" => $password_type,
                    "password" => $password,
                    "code_application" => $code_application,
                    "code_version" => $code_version,
                    "exp" => time() + 3600
                ];

                $jwt = 'sssssssssssss';
                $response['token'] = $jwt;

                return $this->response($response);
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
