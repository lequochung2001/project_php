<?php

namespace App\Client;

use App\BaseController;
use Firebase\JWT\JWT;
use Illuminate\Database\Capsule\Manager as Capsule;

class ClientController extends BaseController
{

    public function __construct(private ClientRepository $clientRepository)
    {
    }

    public function index()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $results = $this->clientRepository->getClients();

                $response = $results;

                return $this->response($response);
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function show($client_id)
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $client = $this->clientRepository->getById($client_id);

                return $this->response($client);
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($client_id)
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $client = $this->clientRepository->getById($client_id);
                $data = [
                    "nom" => isset($_REQUEST['nom']) ? $_REQUEST['nom'] : null,
                    "email" => isset($_REQUEST['email']) ? $_REQUEST['email'] : null,
                    "tel" => isset($_REQUEST['tel']) ? $_REQUEST['tel'] : null,
                    "adresse" => isset($_REQUEST['adresse']) ? $_REQUEST['adresse'] : null,
                    "code_postal" => isset($_REQUEST['code_postal']) ? $_REQUEST['code_postal'] : null,
                    "ville" => isset($_REQUEST['ville']) ? $_REQUEST['ville'] : null,
                ];
              
                $client = Capsule::table('clients')->where('id', $client_id)->update($data);

                return $this->response($client);
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}
