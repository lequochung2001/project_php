<?php

namespace App\Client;

use Illuminate\Database\Capsule\Manager as Capsule;

class ClientRepository
{

    public function getClients()
    {
        try {
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $nom = isset($_GET['nom']) ? $_GET['nom'] : null;
                $ville = isset($_GET['ville']) ? $_GET['ville'] : null;
                $sort = isset($_GET['sort']) ? $_GET['sort'] : null;
                $fields = isset($_GET['fields']) ? $_GET['fields'] : null;
                $limit = isset($_GET['limit']) ? $_GET['limit'] : null;

                $clients = Capsule::table('clients');
                // $clients = $this->searchNom($clients, $nom);
                // $clients = $this->searchVille($clients, $ville);
                // $clients = $this->sort($clients, $sort);
                // if ($fields) {
                //     $clients->select($fields);
                // }
                // if ($limit) {
                //     $clients->limit($limit);
                // }

                return $clients->get();
            }
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function searchNom($clients, $nom)
    {
        return $clients->where(function ($query) use ($nom) {
            $query->where('nom', 'like', '%' . $nom . '%');
        });
    }

    public function searchVille($clients, $ville)
    {
        return $clients->where(function ($query) use ($ville) {
            $query->where('ville', 'like', '%' . $ville . '%');
        });
    }

    public function sort($clients, $sort)
    {
        if ($sort) {
            $sortParams = explode(',', $sort);

            foreach ($sortParams as $param) {
                $direction = 'ASC';

                if ($param[0] === '-') {
                    $direction = 'DESC';
                    $param = substr($param, 1);
                }

                switch ($param) {
                    case 'nom':
                    case 'date_creation':
                    case 'id':
                    case 'ville':
                        $clients = $clients->orderBy($param, $direction);
                        break;
                }
            }
        }
        return $clients;
    }

    public function getById($id)
    {
        return Capsule::table('clients')->where('id', $id)->first();
    }

    public function update($client, $data)
    {
        $client->update($data);
        return $client;
    }
}
